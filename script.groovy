import hudson.model.*
import hudson.plugins.promoted_builds.*

def monthToFilter = env.Month
def format = "MM-yyyy"
def folder = Jenkins.instance.getItemByFullName(env.Folder_Name)
def jobs = folder.getAllItems(hudson.model.Job.class)
println "Build Name, Build#, Env, Build Status, Build Start Time, Build Duration, Build End Time"
for (job in jobs) {
    def builds = job.getBuilds()
    for (build in builds) {
        def buildMonth = new java.text.SimpleDateFormat(format).format(build.getTime())
        if (buildMonth.equals(monthToFilter)) {
            print "${job.fullName},"
            print "${build.number},"
            def parameters = build?.getAllActions().find{ it instanceof ParametersAction }?.parameters
            def envParam = ""
            if(parameters) {
                parameters.each {
                    if(it instanceof  hudson.model.StringParameterValue) {
                        if("environment_name" == it.getName() || "environment" == it.getName()) {
                            envParam = it.getValue()
                        }
                    }
                }
            }
            print "${envParam},"
            print "${build.result},"
            print "${build.getTime()},"
            print "${build.getDurationString()},"
            println "${new Date(build.getTimeInMillis() + build.getDuration())}"
        }
    }
}
